from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from .tokens import account_activation_token,password_reset_token
from django.contrib.auth.models import User
from django.contrib.auth.forms import PasswordResetForm,AuthenticationForm
from django.core.mail import EmailMessage
from django.urls import reverse
from django.contrib.auth import authenticate, login, logout, get_user_model
from .forms import SignupForm
from django.db.models import Q
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
import logging
logger = logging.getLogger('django')

@csrf_exempt
# This code is to make user signup through email verification
def signup(request):
    User = get_user_model()
    if request.method == 'POST':
        form = SignupForm(request.POST)
        email = request.POST.get('email')
        username = request.POST.get('username')
        first_name = request.POST.get('first_name')
        last_name = request.POST.get('last_name')
        password1 = request.POST.get('password1')
        password2 = request.POST.get('password2')

        if email == "" and username != "" and first_name != "" and last_name != "" and password1 != "" and password2 != "":
            data="Expected email field was missing"
            return JsonResponse(data,safe=False,status=400)
        if email != " " and username == "" and first_name != "" and last_name != "" and password1 != "" and password2 != "":
            data="Expected username field was missing"
            return JsonResponse(data,safe=False,status=400)
        if email != "" and username != "" and first_name == "" and last_name != "" and password1 != "" and password2 != "":
            data="Expected firstname field was missing"
            return JsonResponse(data,safe=False,status=400)
        if email != "" and username != "" and first_name != "" and last_name == "" and password1 != "" and password2 != "":
            data="Expected last name field was missing"
            return JsonResponse(data,safe=False,status=400)
        if email != "" and username != "" and first_name != "" and last_name != "" and password1 == "" and password2 != "":
            data="Expected password1 field was missing"
            return JsonResponse(data,safe=False,status=400)
        if email != "" and username != "" and first_name != "" and last_name != "" and password1 != "" and password2 == "":
            data="Expected password2 field was missing"
            return JsonResponse(data,safe=False,status=400)
        #username = request.POST.get('username')
        if User.objects.filter(username=username).exists():
            logger.error("Username already taken")
            data = "Username already taken"    
            return JsonResponse(data,safe=False,status=400)
        #email = request.POST.get('email')
        if User.objects.filter(email=email).exists():
            logger.error("Email already taken")
            data = "Email already taken"    
            return JsonResponse(data,safe=False,status=400)
        if email.split('@')[1] not in ["gmail.com", "yahoo.com", "outlook.com",]:
            logger.error("Please enter an email address with a valid domain")
            data="Please enter an Email Address with a valid domain"
            return JsonResponse(data,safe=False,status=400)
        if password1 != password2:
            logger.error("Both password's doesn't matched or password is less than 8")
            data="Both password's doesn't matched or password is less than 8"
            return JsonResponse(data,safe=False,status=400) 
        if len(password1) != 8:
            logger.error("Password word length must be 8")
            data = "Password length must be 8"
            return JsonResponse(data,safe=False,status=400)   
        if form.is_valid():        
            try:
                user = form.save(commit=False)
                user.is_active = False
                user.save()
                uidb64 = urlsafe_base64_encode(force_bytes(user.pk))
                domain = get_current_site(request).domain
                link=reverse(
                    'activate',kwargs={
                    'uidb64':uidb64, 'token':account_activation_token.make_token(user)
                    }
                    )
                mail_subject = 'Activate your blog account.'
                activate_url= 'http://'+domain+link
                message = 'Hi ' + user.username + 'Please use this link\
                to verify your account\n' + activate_url
                to_email = form.cleaned_data.get('email')
                email = EmailMessage(
                            mail_subject, message, to=[to_email]
                )
                email.send(fail_silently=False)
                logger.info("Please confirm your email address to complete the registration.")
                data = 'Please confirm your email address to complete the registration.'
                return JsonResponse(data,safe=False,status=201)
            except:
                pass       
            
        else:
            logger.error("Form is not valid")
            data = "Form is not valid"   
            return JsonResponse(data,safe=False,status=400) 
    else:
        logger.critical("This method is not allowed")
        data = "method not allows"
        return JsonResponse(data,safe = False,status=405)

# This code is for when the user hits activate link in gmail, it will acivate user's account.
def activate(request, uidb64, token):
    try:
        uidb64 = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uidb64)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        try:
            user.is_active = True
            user.save()
            logger.info("Your account is activated")
            data = "Account is activated"
            return JsonResponse(data,safe=False,status=200)
        except:
            pass    
    else:
        logger.error("Activation link is invalid")
        data = "Activation link is invalid"
        return JsonResponse(data,safe=False,status=400)

# This code is for user login after creating an account for himself.
def Login(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username = username, password = password)
        if user is not None:
            try:
                form = login(request, user)
                data = f"welcome {username}"
                logger.info("You are logged")
                return JsonResponse(data,safe=False,status=200)
            except:
                pass    
        else:
            logger.error("Account doesn't exists.")
            data = "Account doesn't exist, Please Sign Up"
            return JsonResponse(data,safe=False,status=400)
    form = AuthenticationForm()
    data = "Method Not allowed"
    return JsonResponse(data,safe=False,status=405) 

# This code is for user logger out, when he is in logged into his account.
def Logout(request):
    logout(request)
    logger.info("Succesfully logged out")
    data = "Successfully Logged Out" 
    return JsonResponse(data,safe=False,status=200) 

# This code is for user Changing his password to his account, while he is logged in.
def ChangePassword(request):
    if request.method == 'POST':
        username = request.user
        password = request.POST.get('old password')
        u = User.objects.get(username = request.user)
        if u:
            if authenticate(request, username = username, password = password):
                if request.POST.get('new password') == request.POST.get('confirm password'):
                    u.set_password(request.POST.get('new password'))
                    u.save()
                    logger.info("Password is changed successfully")
                    data = "Your Password is successfully changed!!!"
                    return JsonResponse(data,safe=False,status=200)
                else:
                    logger.error("Password's doesn't matched")
                    data =  "New and confirm password do not match"
                    return JsonResponse(data,safe=False,status=400)
            else:
                logger.error("invalid old password")
                data = "invalid old password"
                return JsonResponse(data,safe=False,status=400) 
        else:
            logger.critical("Invalid operation")    
            data = "There is an error!!!"
            return JsonResponse(data,safe=False,status=400)
    else:
        logger.critical("Method is not allowed")
        data = "Metod is not allowed"
        return JsonResponse(data,safe=False,status=405)
   
# This code is to Get User's, who are having accounts.
def userdata(request, id=0, format=None):
    if request.method == 'GET':
        if id == 0:
            logger.info("Registered all user's.")
            data=list(User.objects.values())
            return JsonResponse(data,safe = False,status=200)
 
        elif User.objects.filter(id=id).exists():
            logger.info("Registered user")
            data = list(User.objects.filter(id=id).values())
            return JsonResponse(data,safe = False,status=200)
 
        else:
            logger.error("Invalid id")
            data="No data found or invalid id"
            return JsonResponse(data,safe = False,status=400)
    else:
        logger.critical("Thos method is not allowed")
        data="method not allowed"
        return JsonResponse(data,safe = False,status=405)

# This code is deactivate a user account by his username.
def deluser(request, username):
    if request.method == 'DELETE':
        try:
            u = User.objects.get(username = username)
            u.delete()
            logger.info("user is deleted")
            data = "The user is deleted"
            return JsonResponse(data,safe=False,status=200)
        except:
            logger.error("User is not found")
            data = "user is not found"
            return JsonResponse(data,safe=False,status=400) 
    else:
        logger.critical("method is not allowed")
        data="method not allowed" 
        return JsonResponse(data,safe = False,status=405)   

# This code is reseeting password, while he forget's his password by using email id.
def ResetPassword(request):
    if request.method == "POST":
        form = PasswordResetForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data['email']
            associated_users = User.objects.filter(Q(email=data))
            if associated_users.exists():
                try:
                    for user in associated_users:
                        uidb64 = urlsafe_base64_encode(force_bytes(user.pk))
                        domain = get_current_site(request).domain
                        link = reverse('activate1',kwargs={'uidb64':uidb64, 'token':password_reset_token.make_token(user)})
                        mail_subject = 'Reset your Password.'
                        activate_url= 'http://'+ domain + link
                        message = 'Hi ' + user.username + 'Please use this link \
                        to reset your password\n' + activate_url
                        to_email = form.cleaned_data.get('email')
                        email = EmailMessage(
                                    mail_subject, message, to=[to_email]
                        )
                        email.send(fail_silently=False)
                        logger.info("Please confirm email, to reset password")
                        data = 'Please confirm your email address to Reset your password'
                        return JsonResponse(data,safe=False,status=200)
                except:
                    pass        
            else:
                logger.error("password link is invalid")
                data = "Reset Password link is not valid" 
                return JsonResponse(data,safe=False,status=400)       
        else:
            logger.error("Email is not valid")
            data = "Email is not valid" 
            return JsonResponse(data,safe=False,status=400) 
    else:
        logger.critical("Method is not allowed")
        data = "method is not valid" 
        return JsonResponse(data,safe=False,status=405)   

# This code will redirect reset password portal, when he clicked on email link.
def activate1(request, uidb64, token):
    User = get_user_model()
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and password_reset_token.check_token(user, token):
        if request.POST.get("new password") != None:
            if request.POST.get('new password') == request.POST.get('confirm password'):
                try:
                    user.is_active = True
                    user.set_password(request.POST.get('new password'))
                    user.save()
                    logger.info("Password is changed")
                    data = "Password is changed"
                    return JsonResponse(data,safe=False,status=200)
                except:
                    pass    
            else:
                logger.error("Password doesn't matched")
                data= 'The passwords do not match'
                return JsonResponse(data,safe=False,status=400)
        else:
            logger.error("password is improper")
            data = "Password is improper!!!"
            return JsonResponse(data,safe=False,status=400)
    else:
        logger.critical("There is a problem occur")
        data = "There is a problem occur."
        return JsonResponse(data,safe=False,status=405)     
from django.urls import path
from .views import *

urlpatterns = [
    path('signup', signup),
    path('activate/<slug:uidb64>/<slug:token>/', activate, name='activate'),
    path('activate1/<slug:uidb64>/<slug:token>/', activate1, name='activate1'),
    path('Login', Login),
    path('Logout', Logout),
    path('ChangePassword', ChangePassword),
    path('userdata', userdata),
    path('userdata/<int:id>', userdata),
    path('deluser/<str:username>', deluser),
    path('ResetPassword', ResetPassword),
]
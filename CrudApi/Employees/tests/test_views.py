from userapp.models import User
from django.test import TestCase,Client
from django.urls import reverse
import json
client = Client()
class SignUpPageTests(TestCase):
    def setUp(self):
        pass
    def test_create_valid_userdata(self):
        response = client.get(
            path='/userdata/',
            content_type='application/json')
        self.assertEqual(response.status_code, 200)
    def test_create_valid_userdata_byid(self):
        response = client.get(
            path='/userdata/2',
            content_type='application/json')
        self.assertEqual(response.status_code, 400)
    def test_create_invalid_userdata(self):
        response = client.post(
            path='/userdata/',
            content_type='application/json'
        )
        self.assertEqual(response.status_code, 405)